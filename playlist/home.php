<?php 
include ("header.php");
$id = ["5CTBV3TGgW8","0Xfvk028Kv0","EApnhO2OIrw","IIvSXocE6YY","tucpB76sDvw"];
for($i=0; $i<5; $i++)
{
	$JSON = file_get_contents("https://gdata.youtube.com/feeds/api/videos/{$id[$i]}?v=2&alt=json");
	$JSON_Data = json_decode($JSON);
	//if($i==0)var_dump($JSON_Data->{'entry'});
	$views[$i] = $JSON_Data->{'entry'}->{'yt$statistics'}->{'viewCount'};
	$title[$i] = $JSON_Data->{'entry'}->{'title'}->{'$t'};
}
?>
<div class="container">
	<div class="title">
		<h1 class="artist-name">Trivium</h1>
		<h2 class="playlist-title">More From Trivium</h2>
	</div>
</div>

<div class="container set-height">
	
	<div class="active-iframe">
		<iframe id="now-playing" width="420" height="315" src="//www.youtube.com/embed/<?php echo the_link('http://youtu.be/5CTBV3TGgW8');?>?iv_load_policy=3" frameborder="0" allowfullscreen></iframe>
	</div>

	
	<div class="playlist">
	<?php for($i=0; $i<5;$i++)
	{
	?>
		<div id="song-<?php echo $i;?>" class="song<?php if($i==0) echo ' active';?>">
			<p class="song-name"><?php echo $title[$i];?></p>
			<p class="yt-plays"><?php echo $views[$i];?> views</p>
			<img src="http://img.youtube.com/vi/<?php echo $id[$i];?>/sddefault.jpg" />
			<input class="input-string" type="hidden" value="http://youtu.be/<?php echo $id[$i];?>">
			<div class="border"></div>
		</div>
	<?php
	}
	?>

	</div>
	<div class="fb-share-button" data-href="http://checkoutmycode.com/logan/web/playlist/home.php" data-layout="button_count"></div>
</div>