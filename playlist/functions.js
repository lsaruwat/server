window.addEventListener("load", main);

function main()
{
	selectVideo();
	pwnageness();
}

function selectVideo()
{
	var songs = document.getElementsByClassName("song");
	for(var i=0; i < songs.length; i++)
	{
		songs[i].addEventListener("click", setVideo);
	}
}

function setVideo(e)
{
	var target = e.target;
	if(target.parentNode.className == "song" || target.parentNode.className == "song active")target = target.parentNode;
	if(target.className == "song active") return false;
	active(target);
	var input = target.getElementsByTagName("input")[0];
	var fstring = input.value;
	fstring = the_link(fstring);
	var options = "?iv_load_policy=3&vq=hd720&autoplay=1";
	fstring = "//www.youtube.com/embed/" + fstring + options;
	document.getElementById("now-playing").setAttribute("src", fstring);
}

function active(target)
{
	var songs = document.getElementsByClassName("song");
	for(var i=0; i < songs.length; i++)
	{
		songs[i].setAttribute("class", "song");
	}
	 target.setAttribute("class","song active");
}

function the_link(link)
{
	var substr = link.slice(16);
	return substr;
}

function pwnageness()
{
	var pwnage = document.getElementById("pwnage");
	if(pwnage.className === "init")
	{
		pwnage.setAttribute("class", "blocked");
	}
}